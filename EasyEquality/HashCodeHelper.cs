﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EasyEquality {
    internal static class HashCodeHelper {
        // For more information on why these values are used, see here:
        // https://en.wikipedia.org/wiki/Fowler–Noll–Vo_hash_function
        // https://tools.ietf.org/html/draft-eastlake-fnv-12#section-5
        // http://www.isthe.com/chongo/tech/comp/fnv/index.html#FNV-param
        internal const int Fnv1aOffsetBasis = -0x7ee3623b; // this is the signed value of 0x811c9dc5, which is the value used for the FNV-1a hash algorithm
        internal const int Fnv1aPrime = 16777619;

        internal const int NullValue = 0x6e71fb6c; // random int used for null values

        // uses the FNV-1a algorithm
        public static int GetHashCode<T>(T obj) {
            if (obj == null)
                return NullValue;

            unchecked {
                int hashCode = Fnv1aOffsetBasis;
                var strategy = EasyEquality.GetEqualityStrategy<T>();
                foreach (var typeInfo in strategy.EvaluatedMembers) {
                    if (typeInfo.MemberInfo.MemberType == MemberTypes.Field) {
                        var fieldInfo = (FieldInfo)typeInfo.MemberInfo;
                        object member = fieldInfo.GetValue(obj);
                        hashCode *= Fnv1aPrime;
                        hashCode ^= InnerHashCode(typeInfo.SpecialHandling, member);
                    } else if (typeInfo.MemberInfo.MemberType == MemberTypes.Property) {
                        var propertyInfo = (PropertyInfo)typeInfo.MemberInfo;
                        object member = propertyInfo.GetValue(obj);
                        hashCode *= Fnv1aPrime;
                        hashCode ^= InnerHashCode(typeInfo.SpecialHandling, member);
                    }
                }
                return hashCode;
            }
        }

        private static int InnerHashCode(SpecialHandling specialHandling, object member) {
            if (member == null)
                return NullValue;

            if (specialHandling == SpecialHandling.None) {
                return member.GetHashCode();
            } else if (specialHandling == SpecialHandling.ISet) {
                int hashCode = Fnv1aOffsetBasis;
                var enumerable = (IEnumerable)member;
                foreach (object item in enumerable) {
                    if (item == null) {
                        hashCode ^= NullValue;
                    } else {
                        hashCode ^= item.GetHashCode();
                    }
                }
                return hashCode;
            } else if (specialHandling == SpecialHandling.IDictionary) {
                throw new NotImplementedException();
            } else if (specialHandling == SpecialHandling.IEnumerable) {
                throw new NotImplementedException();
            } else {
                // TODO: real error type
                throw new Exception("Internal error");
            }
        }
    }
}
