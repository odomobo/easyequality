﻿using System.Collections.Generic;

namespace EasyEquality {
    public interface IEqualityStrategy {
        IEnumerable<TypeInfo> EvaluatedMembers { get; }
        IEnumerable<TypeInfo> IgnoredMembers { get; }
    }
}
