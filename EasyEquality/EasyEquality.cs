﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace EasyEquality
{
    public static class EasyEquality
    {
        public static IEqualityStrategy GetEqualityStrategy<T>() {
            // TODO: cache
            return new ImplicitEqualityStrategy(typeof(T));
        }

        public static bool Equals<T>(T object1, object object2) {
            // Quick check, and can sometimes save a lot of calculations.
            // This also checks if both objects are null.
            if (ReferenceEquals(object1, object2))
                return true;

            // if object2 is null, then this will still return false
            if (object1 == null || !(object2 is T))
                return false;

            return EqualsHelper.Equals(object1, (T)object2);
        }

        public static bool Equals<T>(T object1, T object2) {
            // Quick check, and can sometimes save a lot of calculations.
            // This also checks if both objects are null.
            if (ReferenceEquals(object1, object2))
                return true;

            if (object1 == null || object2 == null)
                return false;

            return EqualsHelper.Equals(object1, object2);
        }

        public static int GetHashCode<T>(T obj) {
            if (obj == null)
                return HashCodeHelper.NullValue;

            return HashCodeHelper.GetHashCode(obj);
        }
    }
}
