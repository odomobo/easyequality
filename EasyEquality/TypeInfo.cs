﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace EasyEquality {
    public enum SpecialHandling {
        None,
        ISet,
        IDictionary,
        IEnumerable,
    }

    [DebuggerDisplay("{MemberInfo.MemberType} {Name}")]
    public sealed class TypeInfo : IEquatable<TypeInfo> {
        public string Name { get; }
        public Type Type { get; }
        public string Reason { get; }
        public MemberInfo MemberInfo { get; }
        public SpecialHandling SpecialHandling { get; }

        internal TypeInfo(string name, Type type, string reason, MemberInfo memberInfo, SpecialHandling specialHandling) {
            Name = name;
            Type = type;
            Reason = reason;
            MemberInfo = memberInfo;
            SpecialHandling = specialHandling;
        }
        
        public override bool Equals(object obj) {
            return Equals(obj as TypeInfo);
        }
        
        public bool Equals(TypeInfo other) {
            if (other == null)
                return false;

            return Name == other.Name &&
                   Type == other.Type;
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = Name?.GetHashCode() ?? 0;
                hashCode = (hashCode * 397) ^ (Type?.GetHashCode() ?? 0);
                return hashCode;
            }
        }
    }
}
