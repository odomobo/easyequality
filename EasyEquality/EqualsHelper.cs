﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EasyEquality {
    internal static class EqualsHelper {
        internal static bool Equals<T>(T object1, T object2) {
            // TODO: generate invokable method; cache

            // If any evaluated member in the object does not match, then return false.
            // Otherwise, everything matches, so return true.
            var strategy = EasyEquality.GetEqualityStrategy<T>();
            foreach (var typeInfo in strategy.EvaluatedMembers) {
                if (typeInfo.MemberInfo.MemberType == MemberTypes.Field) {
                    var fieldInfo = (FieldInfo)typeInfo.MemberInfo;
                    object member1 = fieldInfo.GetValue(object1);
                    object member2 = fieldInfo.GetValue(object2);
                    if (!InnerEquality(fieldInfo.FieldType, typeInfo.SpecialHandling, member1, member2))
                        return false;
                } else if (typeInfo.MemberInfo.MemberType == MemberTypes.Property) {
                    var propertyInfo = (PropertyInfo)typeInfo.MemberInfo;
                    object member1 = propertyInfo.GetValue(object1);
                    object member2 = propertyInfo.GetValue(object2);
                    if (!InnerEquality(propertyInfo.PropertyType, typeInfo.SpecialHandling, member1, member2))
                        return false;
                } else {
                    // TODO: provide a real exception
                    throw new Exception("Internal error");
                }
            }
            return true;
        }

        internal const string ISetName = "ISet`1";
        internal const string IDictionaryName = "IDictionary`2";
        internal const string IEnumerableName = "IEnumerable";
        internal const string StringName = "String";
        private static bool InnerEquality(Type type, SpecialHandling specialHandling, object member1, object member2) {
            if (member1 == null && member2 == null)
                return true;

            if (member1 == null || member2 == null)
                return false;

            if (specialHandling == SpecialHandling.None) {
                return member1.Equals(member2);
            } else if (specialHandling == SpecialHandling.ISet) {
                Type iSetType;
                if (type.Name == ISetName) {
                    iSetType = type;
                } else {
                    iSetType = type.GetInterfaces().First(i => i.Name == ISetName);
                }

                var setEqualsMethod = iSetType.GetMethod("SetEquals");
                object ret = setEqualsMethod.Invoke(member1, new object[] { member2 });
                return (bool)ret;
            } else if (specialHandling == SpecialHandling.IDictionary) {
                throw new NotImplementedException();
            } else if (specialHandling == SpecialHandling.IEnumerable) {
                throw new NotImplementedException();
            } else {
                // TODO: real error type
                throw new Exception("Internal error");
            }
        }
    }
}
