using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace EasyEquality {
    public sealed class ImplicitEqualityStrategy : IEqualityStrategy {
        public Type Type { get; }
        public IEnumerable<TypeInfo> EvaluatedMembers { get; }
        public IEnumerable<TypeInfo> IgnoredMembers { get; }

        private static readonly Regex BackingFieldRegex = new Regex(@"^<.*>k__BackingField$");

        internal ImplicitEqualityStrategy(Type type) {
            // TODO: ensure that type is a valid type; must be a class or struct
            Type = type;
            var evaluatedMembers = new List<TypeInfo>();
            EvaluatedMembers = evaluatedMembers;
            var ignoredMembers = new List<TypeInfo>();
            IgnoredMembers = ignoredMembers;

            var allMembers = type.GetMembers(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (var member in allMembers) {
                if (member.MemberType == MemberTypes.Field) {
                    FieldInfo field = (FieldInfo) member;
                    var specialHandling = GetSpecialHandlingForType(field.FieldType);

                    if (!field.IsPublic && BackingFieldRegex.IsMatch(field.Name)) {
                        // don't do anything in the case of backing fields
                    } else if (field.IsStatic) {
                        ignoredMembers.Add(new TypeInfo(field.Name, field.FieldType, "Static field", field, specialHandling));
                    } else if (!field.IsPublic) {
                        ignoredMembers.Add(new TypeInfo(field.Name, field.FieldType, "Non-public field", field, specialHandling));
                    } else {
                        evaluatedMembers.Add(new TypeInfo(field.Name, field.FieldType, "Public field", field, specialHandling));
                    }
                } else if (member.MemberType == MemberTypes.Property) {
                    PropertyInfo property = (PropertyInfo) member;
                    var specialHandling = GetSpecialHandlingForType(property.PropertyType);

                    if (IsStatic(property)) {
                        ignoredMembers.Add(new TypeInfo(property.Name, property.PropertyType, "Static property", property, specialHandling));
                    } else if (!HasPublicGetter(property)) {
                        ignoredMembers.Add(new TypeInfo(property.Name, property.PropertyType, "No public getter", property, specialHandling));
                    } else {
                        evaluatedMembers.Add(new TypeInfo(property.Name, property.PropertyType, "Property with public getter", property, specialHandling));
                    }
                }
            }
        }

        private SpecialHandling GetSpecialHandlingForType(Type type) {
            if (type.Name == EqualsHelper.ISetName || type.GetInterfaces().Any(i => i.Name == EqualsHelper.ISetName)) {
                return SpecialHandling.ISet;
            } else if (type.Name == EqualsHelper.IDictionaryName || type.GetInterfaces().Any(i => i.Name == EqualsHelper.IDictionaryName)) {
                return SpecialHandling.IDictionary;
                // We don't want to provide special handling for string
                // TODO: are there other types that we explicitly don't want to provide special handling for?
            } else if (type.Name != EqualsHelper.StringName && (type.Name == EqualsHelper.IEnumerableName || type.GetInterfaces().Any(i => i.Name == EqualsHelper.IEnumerableName))) {
                return SpecialHandling.IEnumerable;
            } else {
                return SpecialHandling.None;
            }
        }
        
        private static bool IsStatic(PropertyInfo property) {
            if (property.CanRead) {
                var getMethod = property.GetGetMethod(true);
                return getMethod.IsStatic;
            }

            if (property.CanWrite) {
                var setMethod = property.GetSetMethod(true);
                return setMethod.IsStatic;
            }

            // what is a property with no getter or setter?
            return false;
        }

        private bool HasPublicGetter(PropertyInfo property) {
            bool publicGetter = property.CanRead;

            if (publicGetter) {
                var getMethod = property.GetGetMethod(true);
                publicGetter &= getMethod.IsPublic;
            }

            return publicGetter;
        }
    }
}