﻿using System;

namespace EasyEquality {
    public abstract class AutoEquals<T> : IEquatable<T> where T : AutoEquals<T> {
        public override bool Equals(object obj) {
            return EasyEquality.Equals<T>((T)this, obj);
        }

        public bool Equals(T other) {
            return EasyEquality.Equals<T>((T)this, other);
        }

        public static bool operator ==(AutoEquals<T> obj1, AutoEquals<T> obj2) {
            return EasyEquality.Equals<T>((T)obj1, (T)obj2);
        }

        public static bool operator ==(AutoEquals<T> obj1, object obj2) {
            return EasyEquality.Equals<T>((T)obj1, obj2);
        }

        public static bool operator ==(object obj1, AutoEquals<T> obj2) {
            return EasyEquality.Equals<T>((T)obj2, obj1);
        }
        
        public static bool operator !=(AutoEquals<T> obj1, AutoEquals<T> obj2) {
            return !EasyEquality.Equals<T>((T)obj1, (T)obj2);
        }

        public static bool operator !=(AutoEquals<T> obj1, object obj2) {
            return !EasyEquality.Equals<T>((T)obj1, obj2);
        }

        public static bool operator !=(object obj1, AutoEquals<T> obj2) {
            return !EasyEquality.Equals<T>((T)obj2, obj1);
        }

        public override int GetHashCode() {
            return EasyEquality.GetHashCode<T>((T)this);
        }
    }
}
