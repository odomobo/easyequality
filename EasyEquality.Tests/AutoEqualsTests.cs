﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyEquality.Tests.Classes;
using NUnit.Framework;

namespace EasyEquality.Tests {
    [TestFixture]
    class AutoEqualsTests {
        [SetUp]
        public void SetUp() {
        }

        public static IEnumerable<TestCaseData>
            MatchingClass1AutoEqualsObjects
        {
            get
            {
                yield return new TestCaseData(new Class1AutoEquals { A = null }, new Class1AutoEquals { A = null }).SetName("null field");
                yield return new TestCaseData(new Class1AutoEquals { A = "" }, new Class1AutoEquals { A = "" }).SetName("empty string");
                yield return new TestCaseData(new Class1AutoEquals { A = "hello" }, new Class1AutoEquals { A = "hello" }).SetName("\"hello\"");
            }
        }

        public static IEnumerable<TestCaseData>
            NonMatchingClass1AutoEqualsObjects
        {
            get
            {
                yield return new TestCaseData(new Class1AutoEquals { A = null }, new Class1AutoEquals { A = "" }).SetName("null field, empty string");
                yield return new TestCaseData(new Class1AutoEquals { A = "" }, new Class1AutoEquals { A = null }).SetName("empty string, null field");
                yield return new TestCaseData(new Class1AutoEquals { A = "hello" }, new Class1AutoEquals { A = "" }).SetName("\"hello\", empty string");
                yield return new TestCaseData(new Class1AutoEquals { A = "" }, new Class1AutoEquals { A = "hello" }).SetName("empty string, \"hello\"");
                yield return new TestCaseData(new Class1AutoEquals { A = "" }, null).SetName("empty string, null object");
            }
        }

        [Test]
        [TestCaseSource(nameof(MatchingClass1AutoEqualsObjects))]
        public void OverriddenEqualsShouldReturnTrue_ForClass1AutoEquals_WhenMatchingFields(Class1AutoEquals obj1, Class1AutoEquals obj2) {
            // arrange
            // act
            bool eq = obj1.Equals((object)obj2);
            // assume
            Assume.That(EasyEquality.Equals(obj1, (object)obj2), Is.True);
            // assert
            Assert.IsTrue(eq);
        }

        [Test]
        [TestCaseSource(nameof(NonMatchingClass1AutoEqualsObjects))]
        public void OverriddenEqualsShouldReturnFalse_ForClass1AutoEquals_WhenNonMatchingFields(Class1AutoEquals obj1, Class1AutoEquals obj2) {
            // arrange
            // act
            bool eq = obj1.Equals((object)obj2);
            // assume
            Assume.That(EasyEquality.Equals(obj1, (object)obj2), Is.False);
            // assert
            Assert.IsFalse(eq);
        }

        [Test]
        [TestCaseSource(nameof(MatchingClass1AutoEqualsObjects))]
        public void IEquatableEqualsShouldReturnTrue_ForClass1AutoEquals_WhenMatchingFields(Class1AutoEquals obj1, Class1AutoEquals obj2) {
            // arrange
            // act
            bool eq = obj1.Equals(obj2);
            // assume
            Assume.That(EasyEquality.Equals(obj1, obj2), Is.True);
            // assert
            Assert.IsTrue(eq);
        }

        [Test]
        [TestCaseSource(nameof(NonMatchingClass1AutoEqualsObjects))]
        public void IEquatableEqualsShouldReturnFalse_ForClass1AutoEquals_WhenNonMatchingFields(Class1AutoEquals obj1, Class1AutoEquals obj2) {
            // arrange
            // act
            bool eq = obj1.Equals(obj2);
            // assume
            Assume.That(EasyEquality.Equals(obj1, obj2), Is.False);
            // assert
            Assert.IsFalse(eq);
        }

        [Test]
        [TestCaseSource(nameof(MatchingClass1AutoEqualsObjects))]
        public void EqualsOperatorShouldReturnTrue_ForClass1AutoEquals_WhenMatchingFields(Class1AutoEquals obj1, Class1AutoEquals obj2) {
            // arrange
            var obj1o = (object)obj1;
            var obj2o = (object)obj2;
            // act
            bool eq1 = obj1 == obj2;
            bool eq2 = obj1o == obj2;
            bool eq3 = obj1 == obj2o;
            // assume
            Assume.That(EasyEquality.Equals(obj1, (object)obj2), Is.True);
            // assert
            Assert.IsTrue(eq1);
            Assert.IsTrue(eq2);
            Assert.IsTrue(eq3);
        }

        [Test]
        [TestCaseSource(nameof(NonMatchingClass1AutoEqualsObjects))]
        public void EqualsOperatorShouldReturnFalse_ForClass1AutoEquals_WhenNonMatchingFields(Class1AutoEquals obj1, Class1AutoEquals obj2) {
            // arrange
            var obj1o = (object)obj1;
            var obj2o = (object)obj2;
            // act
            bool eq1 = obj1 == obj2;
            bool eq2 = obj1o == obj2;
            bool eq3 = obj1 == obj2o;
            // assume
            Assume.That(EasyEquality.Equals(obj1, (object)obj2), Is.False);
            // assert
            Assert.IsFalse(eq1);
            Assert.IsFalse(eq2);
            Assert.IsFalse(eq3);
        }

        [Test]
        [TestCaseSource(nameof(MatchingClass1AutoEqualsObjects))]
        public void NotEqualsOperatorShouldReturnFalse_ForClass1AutoEquals_WhenMatchingFields(Class1AutoEquals obj1, Class1AutoEquals obj2) {
            // arrange
            var obj1o = (object) obj1;
            var obj2o = (object) obj2;
            // act
            bool eq1 = obj1 != obj2;
            bool eq2 = obj1o != obj2;
            bool eq3 = obj1 != obj2o;
            // assume
            Assume.That(EasyEquality.Equals(obj1, (object)obj2), Is.True);
            // assert
            Assert.IsFalse(eq1);
            Assert.IsFalse(eq2);
            Assert.IsFalse(eq3);
        }

        [Test]
        [TestCaseSource(nameof(NonMatchingClass1AutoEqualsObjects))]
        public void NotEqualsOperatorShouldReturnTrue_ForClass1AutoEquals_WhenNonMatchingFields(Class1AutoEquals obj1, Class1AutoEquals obj2) {
            // arrange
            var obj1o = (object)obj1;
            var obj2o = (object)obj2;
            // act
            bool eq1 = obj1 != obj2;
            bool eq2 = obj1o != obj2;
            bool eq3 = obj1 != obj2o;
            // assume
            Assume.That(EasyEquality.Equals(obj1, (object)obj2), Is.False);
            // assert
            Assert.IsTrue(eq1);
            Assert.IsTrue(eq2);
            Assert.IsTrue(eq3);
        }

        [Test]
        public void EqualsOperatorShouldReturnFalse_WhenComparingClass1AutoEqualsAgainstOtherType() {
            // arrange
            var obj1 = new Class1AutoEquals {A = "Hello"};
            var obj2 = new object();
            // act
            bool eq1 = obj1 == obj2;
            bool eq2 = obj2 == obj1;
            // assert
            Assert.IsFalse(eq1);
            Assert.IsFalse(eq2);
        }

        [Test]
        public void NotEqualsOperatorShouldReturnFalse_WhenComparingClass1AutoEqualsAgainstOtherType() {
            // arrange
            var obj1 = new Class1AutoEquals { A = "Hello" };
            var obj2 = new object();
            // act
            bool eq1 = obj1 != obj2;
            bool eq2 = obj2 != obj1;
            // assert
            Assert.IsTrue(eq1);
            Assert.IsTrue(eq2);
        }


        [Test]
        public void EqualsOperatorShouldReturnTrue_WhenComparingNullAgainstNull_RegardlessOfType() {
            // arrange
            Class1AutoEquals nullAutoEquals = null;
            object nullObject = null;
            string nullString = null;
            // act
            // disable warning for comparing object against itself
#pragma warning disable CS1718
            bool eq1 = nullAutoEquals == nullAutoEquals;
#pragma warning restore CS1718
            bool eq2 = nullAutoEquals == nullObject;
            bool eq3 = nullAutoEquals == nullString;
            bool eq4 = nullObject == nullAutoEquals;
            bool eq5 = nullString == nullAutoEquals;
            // assert
            Assert.IsTrue(eq1);
            Assert.IsTrue(eq2);
            Assert.IsTrue(eq3);
            Assert.IsTrue(eq4);
            Assert.IsTrue(eq5);
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase("hello")]
        [TestCase("A fairly complex string.")]
        public void HashCodesShouldGenerateCorrectly_ForClass1AutoEquals(string value) {
            // arrange
            var obj1 = new Class1AutoEquals {A = value};
            // act
            int hashCode = obj1.GetHashCode();
            // assert
            Assert.AreEqual(EasyEquality.GetHashCode(obj1), hashCode);
        }
    }
}
