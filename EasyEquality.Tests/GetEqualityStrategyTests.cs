﻿using System.Linq;
using NUnit.Framework;
using EasyEquality.Tests.Classes;

namespace EasyEquality.Tests {
    [TestFixture]
    class GetEqualityStrategyTests {
        [SetUp]
        public void SetUp() {}

        [Test]
        public void ShouldHaveNoMembers_WhenEmptyClass() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<ClassEmpty>();
            // assert
            Assert.IsInstanceOf<ImplicitEqualityStrategy>(equalityStrategy);
            Assert.AreEqual(0, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(0, equalityStrategy.IgnoredMembers.Count());
        }

        [Test]
        public void ShouldHave1EvaluatedField_AndNoIgnoredMembers_WhenClass1PublicField() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<Class1PublicField>();
            // assert
            Assert.IsInstanceOf<ImplicitEqualityStrategy>(equalityStrategy);
            Assert.AreEqual(1, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(0, equalityStrategy.IgnoredMembers.Count());
        }

        [Test]
        public void ShouldHave2EvaluatedFields_AndNoIgnoredMembers_WhenClass2PublicFields() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<Class2PublicFields>();
            // assert
            Assert.IsInstanceOf<ImplicitEqualityStrategy>(equalityStrategy);
            Assert.AreEqual(2, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(0, equalityStrategy.IgnoredMembers.Count());
        }

        [Test]
        public void ShouldHave2EvaluatedMembers_And2IgnoredMembers_WhenClass1Public1PrivateField_And1Public1PrivateProperty() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<Class1Public1PrivateField_And1Public1PrivateProperty>();
            // assert
            Assert.IsInstanceOf<ImplicitEqualityStrategy>(equalityStrategy);
            Assert.AreEqual(2, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(2, equalityStrategy.IgnoredMembers.Count());
        }

        [Test]
        public void ShouldHaveNoEvaluatedMembers_And1IgnoredMembers_WhenClass1PublicStaticProperty() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<Class1PublicStaticProperty>();
            // assert
            Assert.IsInstanceOf<ImplicitEqualityStrategy>(equalityStrategy);
            Assert.AreEqual(0, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(1, equalityStrategy.IgnoredMembers.Count());
        }

        [Test]
        public void ShouldHaveNoEvaluatedMembers_And1IgnoredMember_WhenClass1PublicStaticField() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<Class1PublicStaticField>();
            // assert
            Assert.IsInstanceOf<ImplicitEqualityStrategy>(equalityStrategy);
            Assert.AreEqual(0, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(1, equalityStrategy.IgnoredMembers.Count());
        }

        [Test]
        public void ShouldHaveNoEvaluatedMembers_And1IgnoredMember_WhenClass1Property_PublicSetterPrivateGetter() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<Class1Property_PublicSetterPrivateGetter>();
            // assert
            Assert.IsInstanceOf<ImplicitEqualityStrategy>(equalityStrategy);
            Assert.AreEqual(0, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(1, equalityStrategy.IgnoredMembers.Count());
        }

        [Test]
        public void ShouldHave1EvaluatedMember_AndNoIgnoredMembers_WhenClass1Property_PublicGetterPrivateSetter() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<Class1Property_PublicGetterPrivateSetter>();
            // assert
            Assert.IsInstanceOf<ImplicitEqualityStrategy>(equalityStrategy);
            Assert.AreEqual(1, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(0, equalityStrategy.IgnoredMembers.Count());
        }

        [Test]
        public void ShouldHave10EvaluatedMembers_And3IgnoredMembers_WhenClassComplex1() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<ClassComplex1>();
            // assert
            Assert.IsInstanceOf<ImplicitEqualityStrategy>(equalityStrategy);
            Assert.AreEqual(10, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(3, equalityStrategy.IgnoredMembers.Count());
        }

        [Test]
        public void ShouldUseIDictionaryHandling_ForIDictionary() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<Class1IDictionary>();
            // assert
            Assert.AreEqual(1, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(0, equalityStrategy.IgnoredMembers.Count());
            Assert.AreEqual(SpecialHandling.IDictionary, equalityStrategy.EvaluatedMembers.First().SpecialHandling);
        }

        [Test]
        public void ShouldUseIDictionaryHandling_ForDictionary() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<Class1Dictionary>();
            // assert
            Assert.AreEqual(1, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(0, equalityStrategy.IgnoredMembers.Count());
            Assert.AreEqual(SpecialHandling.IDictionary, equalityStrategy.EvaluatedMembers.First().SpecialHandling);
        }

        [Test]
        public void ShouldUseIEnumerableHandling_ForIEnumerable() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<Class1IEnumerable>();
            // assert
            Assert.AreEqual(1, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(0, equalityStrategy.IgnoredMembers.Count());
            Assert.AreEqual(SpecialHandling.IEnumerable, equalityStrategy.EvaluatedMembers.First().SpecialHandling);
        }

        [Test]
        public void ShouldUseIEnumerableHandling_ForList() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<Class1List>();
            // assert
            Assert.AreEqual(1, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(0, equalityStrategy.IgnoredMembers.Count());
            Assert.AreEqual(SpecialHandling.IEnumerable, equalityStrategy.EvaluatedMembers.First().SpecialHandling);
        }

        [Test]
        public void ShouldUseIEnumerableHandling_ForIEnumerableNonGeneric() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<Class1IEnumerableNonGeneric>();
            // assert
            Assert.AreEqual(1, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(0, equalityStrategy.IgnoredMembers.Count());
            Assert.AreEqual(SpecialHandling.IEnumerable, equalityStrategy.EvaluatedMembers.First().SpecialHandling);
        }

        [Test]
        public void ShouldUseIEnumerableHandling_ForArrayListNonGeneric() {
            // arrange
            // act
            var equalityStrategy = EasyEquality.GetEqualityStrategy<Class1ArrayListNonGeneric>();
            // assert
            Assert.AreEqual(1, equalityStrategy.EvaluatedMembers.Count());
            Assert.AreEqual(0, equalityStrategy.IgnoredMembers.Count());
            Assert.AreEqual(SpecialHandling.IEnumerable, equalityStrategy.EvaluatedMembers.First().SpecialHandling);
        }

        // TODO: test more cases

        // TODO: test structs
    }
}