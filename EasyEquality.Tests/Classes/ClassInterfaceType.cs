﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyEquality.Tests.Classes {


    class ClassInterfaceType {
        public IFoo A;

        public interface IFoo { }

        public class Foo : IFoo {
            public int A;

            public override bool Equals(object obj) {
                return Equals(obj as Foo);
            }
            
            public bool Equals(Foo foo) {
                if (foo == null)
                    return false;

                return A == foo.A;
            }

            public override int GetHashCode() {
                return A;
            }
        }
    }
}
