﻿// disable "never assigned to" warning
#pragma warning disable CS0649

namespace EasyEquality.Tests.Classes {
    class Class1AutoEquals : AutoEquals<Class1AutoEquals> {
        public string A;
    }
}
