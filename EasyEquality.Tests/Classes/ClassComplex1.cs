﻿// disable "never assigned to" and "never used" warnings
#pragma warning disable CS0649, CS0169

namespace EasyEquality.Tests.Classes {
    class ClassComplex1 {
        public string First;
        private int _second;
        public bool third { get; }
        public string _fourth;

        public int _fifthBackingField;
        public int Sixth {
            get { return _fifthBackingField; }
        }

        protected string Seventh;
        protected string _eighth { get; set; }

        public string Ninth { get; set; }

        public string C;
        public int D;
        public string A;
        public int B;
    }
}
