﻿namespace EasyEquality.Tests.Classes {
    class Class1Public1PrivateField_And1Public1PrivateProperty {
        public int A;
        private bool _b;
        public string J { get; set; }
        private string K { get; set; }

        public Class1Public1PrivateField_And1Public1PrivateProperty(int a, bool b, string j, string k) {
            A = a;
            _b = b;
            J = j;
            K = k;
        }

        public override bool Equals(object obj) {
            return EasyEquality.Equals(this, obj);
        }

        public override int GetHashCode() {
            return EasyEquality.GetHashCode(this);
        }
    }
}
