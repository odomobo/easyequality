﻿using System.Collections.Generic;
using EasyEquality.Tests.Classes;
using NUnit.Framework;

namespace EasyEquality.Tests {
    [TestFixture]
    class EqualsTests {
        [SetUp]
        public void SetUp() {
            
        }

        public static IEnumerable<TestCaseData>
            SourceFor_ShouldReturnTrue_WhenMatchingPublicMembers_ForClass1Public1PrivateField_And1Public1PrivateProperty {
            get {
                yield return new TestCaseData(new Class1Public1PrivateField_And1Public1PrivateProperty(10, true, "Hi", "There"), new Class1Public1PrivateField_And1Public1PrivateProperty(10, true, "Hi", "There")).SetName("All fields matching");
                yield return new TestCaseData(new Class1Public1PrivateField_And1Public1PrivateProperty(10, true, "Hi", "There"), new Class1Public1PrivateField_And1Public1PrivateProperty(10, false, "Hi", "Bob")).SetName("Evaluated fields matching; ignored fields not matching");
                // TODO: more test cases
            }
        }

        [Test]
        [TestCaseSource(nameof(SourceFor_ShouldReturnTrue_WhenMatchingPublicMembers_ForClass1Public1PrivateField_And1Public1PrivateProperty))]
        public void ShouldReturnTrue_WhenMatchingPublicMembers_ForClass1Public1PrivateField_And1Public1PrivateProperty(Class1Public1PrivateField_And1Public1PrivateProperty obj1, Class1Public1PrivateField_And1Public1PrivateProperty obj2) {
            // arrange
            // act
            bool eq = EasyEquality.Equals(obj1, obj2);
            // assert
            Assert.IsTrue(eq);
        }

        [Test]
        public void ShouldReturnTrue_WhenMatching_AndFieldIsInterfaceType() {
            // arrange
            var obj1 = new ClassInterfaceType {
                A = new ClassInterfaceType.Foo {A = 10}
            };
            var obj2 = new ClassInterfaceType {
                A = new ClassInterfaceType.Foo {A = 10}
            };
            // act
            bool eq = EasyEquality.Equals(obj1, obj2);
            // assert
            Assert.IsTrue(eq);
        }

        public static IEnumerable<TestCaseData>
            SourceFor_ShouldReturnTrue_WhenMatchingISets_WithDifferentImplementations
        {
            get
            {
                yield return new TestCaseData(new Class1ISet {A = new HashSet<string> { "B", "A" } }, new Class1ISet { A = new SortedSet<string> { "B", "A" } });
                // TODO: more test cases
            }
        }

        [Test]
        [TestCaseSource(nameof(SourceFor_ShouldReturnTrue_WhenMatchingISets_WithDifferentImplementations))]
        public void ShouldReturnTrue_WhenMatchingISets_WithDifferentImplementations(Class1ISet obj1, Class1ISet obj2) {
            // arrange
            // act
            bool eq = EasyEquality.Equals(obj1, obj2);
            // assert
            Assert.IsTrue(eq);
        }

        public static IEnumerable<TestCaseData>
            SourceFor_ShouldReturnTrue_WhenMatchingHashSets_WithDifferentOrder
        {
            get
            {
                yield return new TestCaseData(new Class1HashSet { A = new HashSet<string> { "A", "B" } }, new Class1HashSet { A = new HashSet<string> { "B", "A" } });
                // TODO: more test cases
            }
        }

        [Test]
        [TestCaseSource(nameof(SourceFor_ShouldReturnTrue_WhenMatchingHashSets_WithDifferentOrder))]
        public void ShouldReturnTrue_WhenMatchingHashSets_WithDifferentOrder(Class1HashSet obj1, Class1HashSet obj2) {
            // arrange
            // act
            bool eq = EasyEquality.Equals(obj1, obj2);
            // assert
            Assert.IsTrue(eq);
        }

        // TODO: more tests
    }
}
