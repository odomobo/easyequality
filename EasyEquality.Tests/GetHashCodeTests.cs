﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyEquality.Tests.Classes;
using NUnit.Framework;

namespace EasyEquality.Tests {
    [TestFixture]
    class GetHashCodeTests {
        [SetUp]
        public void SetUp() {
            
        }

        [Test]
        public void ShouldHaveHashCode0_WhenEmptyClass() {
            // arrange
            var obj = new ClassEmpty();
            // act
            int hashCode = EasyEquality.GetHashCode(obj);
            // assert
            Assert.AreEqual(CalculateFnv1aHash(new int[] {}), hashCode);
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase("A")]
        [TestCase("Here is a non-trivial string. Enjoy!")]
        public void HashShouldMatchMemberHash_When1Field(string value) {
            // arrange
            var obj = new Class1PublicField {A = value};
            // act
            int hashCode = EasyEquality.GetHashCode(obj);
            // assert
            int hash1 = value?.GetHashCode() ?? HashCodeHelper.NullValue;
            Assert.AreEqual(CalculateFnv1aHash(new []{hash1}), hashCode);
        }

        /// <summary>
        /// This makes the assumption that all fields are evaluated in the order which they appear in the class. This assumption is based on the current implementation of Type.GetFields(), but this could fail
        /// if this ever changes in the future.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        [Test]
        [TestCase(null, null)]
        [TestCase(null, "")]
        [TestCase("", null)]
        [TestCase("", "")]
        [TestCase(null, "Here is a non-trivial string. Enjoy!")]
        [TestCase("Here is a non-trivial string. Enjoy!", null)]
        [TestCase("Here is a non-trivial string. Enjoy!", "And here is another string.")]
        public void HashShouldMatchMemberHash_When2Fields(string value1, string value2) {
            // arrange
            var obj = new Class2PublicFields { A = value1, B = value2 };
            // act
            int hashCode = EasyEquality.GetHashCode(obj);
            // assert
            int hash1 = value1?.GetHashCode() ?? HashCodeHelper.NullValue;
            int hash2 = value2?.GetHashCode() ?? HashCodeHelper.NullValue;
            Assert.AreEqual(CalculateFnv1aHash(new[] { hash1, hash2 }), hashCode);
        }

        public static IEnumerable<TestCaseData>
            SourceFor_ShouldReturnSameHashCode_WhenDifferentSetsWithSameValues
        {
            get
            {
                yield return new TestCaseData(new Class1ISet { A = null }, new Class1ISet { A = null });
                yield return new TestCaseData(new Class1ISet { A = new HashSet<string> {} }, new Class1ISet { A = new HashSet<string> {} });
                yield return new TestCaseData(new Class1ISet { A = new HashSet<string> { "A", "B" } }, new Class1ISet { A = new HashSet<string> { "B", "A" } });
                yield return new TestCaseData(new Class1ISet { A = new SortedSet<string> { "Anna", "Bob", "Charlie", "Diana", "Edgar" } }, new Class1ISet { A = new HashSet<string> { "Edgar", "Diana", "Charlie", "Bob", "Anna" } });
            }
        }

        [Test]
        [TestCaseSource(nameof(SourceFor_ShouldReturnSameHashCode_WhenDifferentSetsWithSameValues))]
        public void ShouldReturnSameHashCode_WhenDifferentSetsWithSameValues(Class1ISet obj1, Class1ISet obj2) {
            // arrange
            // act
            int hashCode1 = EasyEquality.GetHashCode(obj1);
            int hashCode2 = EasyEquality.GetHashCode(obj2);
            // assert
            Assert.AreEqual(hashCode1, hashCode2);
        }

        [Test]
        public void ShouldReturnDifferentHashCode_ForClass1ISet_WhenNullVsEmptySet() {
            // arrange
            var obj1 = new Class1ISet { A = null };
            var obj2 = new Class1ISet { A = new HashSet<string>() };
            // act
            int hashCode1 = EasyEquality.GetHashCode(obj1);
            int hashCode2 = EasyEquality.GetHashCode(obj2);
            // assert
            Assert.AreNotEqual(hashCode1, hashCode2);
        }

        [Test]
        public void ShouldReturnDifferentHashCode_ForClass1ISet_WithVsWithoutNullInSet() {
            // arrange
            var obj1 = new Class1ISet { A = new HashSet<string> { "A", "B" } };
            var obj2 = new Class1ISet { A = new HashSet<string> { "A", "B", null } };
            // act
            int hashCode1 = EasyEquality.GetHashCode(obj1);
            int hashCode2 = EasyEquality.GetHashCode(obj2);
            // assert
            Assert.AreNotEqual(hashCode1, hashCode2);
        }

        [Test]
        public void EnsureOffsetBasisIsCorrect() {
            // arrange
            // act
            // assert
            unchecked {
                uint officialFnv1aOffsetBasis = 0x811c9dc5;
                Assert.AreEqual((int)officialFnv1aOffsetBasis, HashCodeHelper.Fnv1aOffsetBasis);
            }
        }

        private int CalculateFnv1aHash(int[] hashes) {
            unchecked {
                int hashCode = HashCodeHelper.Fnv1aOffsetBasis;
                foreach (int hash in hashes) {
                    hashCode *= HashCodeHelper.Fnv1aPrime;
                    hashCode ^= hash;
                }
                return hashCode;
            }
        }
    }
}
