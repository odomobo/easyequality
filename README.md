## Milestones for 1.0 ##

* GetEqualityStrategy()
* Equals()
* GetHashCode()

## Required features for 1.0 ##

* ~~Implicit strategy (public fields and properties)~~
* Explicit strategy
* Data contract strategy
* Full unit testing
* Thread safety
* Code generation with caching per type
* Inspectable, with explanation why did or did not include particular members
* (pseudo) code generation, to see how Equals() or GetHashCode() are being generated
* Collection libraries automatically use semi-smart comparison
    * ~~ISet~~
    * IDictionary
    * What to do about Non-generic IDictionary?
    * IEnumerable
* Semi-smart comparison can be overridden by default equality comparison, with explicit strategy annotations

## Milestones for 2.0 ##

* ~~CompareTo()~~
* ToString()
* ~~CreateIEqualityComparitor()~~

Should we really do comparisons? I mean, what does it mean to compare 2 dictionaries or sets to each other?

Maybe ToString should be a separate project altogether.

## Required features for 2.0 ##

* Members need to be ordered in order of declaration, by default
* Alternate ordering schemes?
* ToString should use sane default, and should be fairly flexible
